# Jasonette

This pod is simply using jasonette project to make it simpilified usage.

## Main project URL 
* [Project URL](https://docs.jasonette.com/) 
* [Advanced usage](https://docs.jasonette.com/advanced/) 


###  Getting Started
Here's how you can open a JasonViewController from your own viewcontroller—it's the same as opening any other viewcontrollers.

#### 1- First, include JasonViewController.h in your viewcontroller
```
#import "JasonViewController.h"
```

#### 2- Open with push transition
To open the JasonViewController with a push transition, do this:
```
JasonViewController *vc = [[JasonViewController alloc] init];
vc.url = @"https://jasonbase.com/things/jYJ.json";
[self.navigationViewController pushViewController:vc animated:YES];
```

#### 3- Open with modal transition 
To open JasonViewController as a modal, do this:
```
JasonViewController *vc = [[JasonViewController alloc] init];
vc.url = @"https://jasonbase.com/things/jYJ.json";
UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
[self.navigationController presentViewController:nav animated:YES completion:nil];
```









