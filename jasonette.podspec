Pod::Spec.new do |s|
  s.name             = 'jasonette'
  s.version          = "0.0.6"
  s.summary          = "NATIVE APP OVER JSON MARKUP"
  s.description      = <<-DESC
  Jasonette is an already "pre-cooked" app, which means "getting started" means simply downloading and running the app.
  DESC

  s.homepage         = 'https://jasonette.com/'
  s.author           = { "Mohammed Gomaa" => "eng.mhmdgoma@yahoo.com" }
  s.source           = { :git => 'https://mhmdgoma@bitbucket.org/mhmdgoma/jasonette.git', :tag => s.version.to_s }
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.ios.deployment_target = '9.0'
  s.platform         = :ios, '9.0'
  s.source_files     = 'app/**/*.{h,m,swift,xcassets,png,js,pch}'

  s.dependency 'UICKeyChainStore'
  s.dependency 'TWMessageBarManager'
  s.dependency 'AFNetworking'
  s.dependency 'SWTableViewCell'
  s.dependency 'APAddressBook'
  s.dependency 'TTTAttributedLabel'
  s.dependency 'BBBadgeBarButtonItem'
  s.dependency 'IQAudioRecorderController'
  s.dependency 'REMenu'
  s.dependency 'JDStatusBarNotification'
  s.dependency 'HMSegmentedControl'
  s.dependency "SWFrameButton"
  s.dependency 'libPhoneNumber-iOS'
  s.dependency "NSGIF", "~> 1.0"
  s.dependency 'INTULocationManager'
  s.dependency "AHKActionSheet"
  s.dependency 'TDOAuth'
  s.dependency "AFOAuth2Manager"
  s.dependency "CYRTextView"
  s.dependency 'FreeStreamer'
  s.dependency 'SDWebImage', '~> 3.8.1'
  s.dependency 'SZTextView'
  s.dependency 'SBJson', '~> 4.0.2'
  s.dependency 'DHSmartScreenshot'
  s.dependency 'NSHash'
  s.dependency 'JSCoreBom'
  s.dependency "RMDateSelectionViewController"
  s.dependency 'DTCoreText'
  s.dependency 'PHFComposeBarView'
  s.dependency 'DAKeyboardControl'
  s.dependency 'MBProgressHUD'
  s.dependency 'FLEX'
  s.dependency 'SocketRocket'

end


